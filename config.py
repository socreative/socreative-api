# Where you need the plugins to be stored.
PLUGINS_BASE_DIR = "/var/ossec/plugins"

# Where is the shared directory.
AGENTS_SHARED_DIR = "/var/ossec/etc/shared/default/plugins"

# Setting up the paths for the certificates 
SSL_KEYFILE_PATH = "/etc/socreative-api/socreative-api.key"
SSL_CERTFILE_PATH = "/etc/socreative-api/socreative-api.pem"

# Where to install the decoders and rules.
RULES_FILE_PATH = "/var/ossec/etc/rules/local_rules.xml"
DECODERS_FILE_PATH = "/var/ossec/etc/decoders/local_decoder.xml"

ACTIVE_RESPONSE_PATH = "/var/ossec/active-response/plugins"

TEMPLATE_PLUGIN_PATH = "/opt/socreative-api/assets/template-plugin.zip"

FULL_PLUGIN_DIR_STRUCTURE = [
    "active-response/script.py",
    "decoders.xml",
    "metadata.json",
    "rules.xml",
    "script.py",
]

# These are the files that needs to be at the agent.
MINIMAL_PLUGIN_DIR_STRUCTURE = [
    "script.py",
    "metadata.json",
]
