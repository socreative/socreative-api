import asyncio
from zipfile import ZipFile

from fastapi.exceptions import HTTPException
from models import PluginMetadata
import os
import json
import re
import shutil
from typing import List

from config import (
    ACTIVE_RESPONSE_PATH,
    AGENTS_SHARED_DIR,
    FULL_PLUGIN_DIR_STRUCTURE,
    PLUGINS_BASE_DIR,
    RULES_FILE_PATH,
    DECODERS_FILE_PATH,
)


def add_plugin_to_agents(plugin_id: str, version: str, agents: List[str]):
    for agent_id in agents:
        agent_file_path = f"{AGENTS_SHARED_DIR}/{agent_id}.json"

        agent_plugins = []
        # If there is already a file for this agent load it.
        if os.path.isfile(agent_file_path):
            with open(agent_file_path) as file:
                agent_plugins = json.loads(file.read())

        for plugin in agent_plugins:
            if plugin["id"] == plugin_id:
                # The plugin is already in the agent's list.
                # Just change the version so the agent picks the changes.
                plugin["version"] = version
                break
        else:
            # The plugin is not in the agent's list.
            # Add the plugin to the agent's list.
            agent_plugins.append({"id": plugin_id, "version": version})

        # Rewrite the plugins list to agent file.
        with open(agent_file_path, "w") as file:
            file.write(json.dumps(agent_plugins, indent=2))


def remove_plugin_from_agents(plugin_id: str, agents: List[str]):
    for agent_id in agents:
        agent_file_path = f"{AGENTS_SHARED_DIR}/{agent_id}.json"

        # The agent doesn't have a file.
        if not os.path.isfile(agent_file_path):
            continue

        with open(agent_file_path) as file:
            agent_plugins = json.loads(file.read())
            agent_plugins = list(
                filter(lambda entry: entry["id"] != plugin_id, agent_plugins)
            )

        # Rewrite the plugins list to agent file.
        with open(agent_file_path, "w") as file:
            file.write(json.dumps(agent_plugins, indent=2))


def add_plugin_decoder(plugin_id: str):
    plugin_decoder_path = f"{PLUGINS_BASE_DIR}/{plugin_id}/decoders.xml"

    with open(plugin_decoder_path) as plugin_decoder_file:
        plugin_decoder = "\n".join(
            [
                f"<!-- START: PLUGIN {plugin_id} -->",
                plugin_decoder_file.read(),
                f"<!-- END: PLUGIN {plugin_id} -->",
            ]
        )

        with open(DECODERS_FILE_PATH, "a") as decoders_file:
            decoders_file.write(plugin_decoder + "\n")


def remove_plugin_decoder(plugin_id: str):
    with open(DECODERS_FILE_PATH) as decoders_file:
        decoders = decoders_file.read()
        result = re.sub(
            f"<!-- START: PLUGIN {plugin_id} -->.*?<!-- END: PLUGIN {plugin_id} -->",
            "",
            decoders,
            flags=re.DOTALL,
        )

    with open(DECODERS_FILE_PATH, "w") as decoders_file:
        decoders_file.write(result)


def add_plugin_rule(plugin_id: str):
    plugin_rule_path = f"{PLUGINS_BASE_DIR}/{plugin_id}/rules.xml"

    with open(plugin_rule_path) as plugin_rule_file, \
        open(RULES_FILE_PATH) as rules_file:

        plugin_rule = "\n".join(
            [
                f"<!-- START: PLUGIN {plugin_id} -->",
                plugin_rule_file.read(),
                f"<!-- END: PLUGIN {plugin_id} -->",
            ]
        )

        rules = rules_file.read()
        modified_rules = re.sub(r"</group>\s*$", f"{plugin_rule}\n</group>\n", rules)

    with open(RULES_FILE_PATH, "w") as rules_file:
        rules_file.write(modified_rules)


def remove_plugin_rule(plugin_id: str):
    with open(RULES_FILE_PATH) as rules_file:
        rules = rules_file.read()
        modified_rules = re.sub(
            f"<!-- START: PLUGIN {plugin_id} -->.*?<!-- END: PLUGIN {plugin_id} -->",
            "",
            rules,
            flags=re.DOTALL,
        )

    with open(RULES_FILE_PATH, "w") as rules_file:
        rules_file.write(modified_rules)


def add_plugin_active_response(plugin_id: str):
    ar_path = f"{PLUGINS_BASE_DIR}/{plugin_id}/active-response"
    ar_script_path = f"{ar_path}/script.py"

    # Copy active response script and make it executable.
    shutil.copyfile(ar_script_path, f"{ACTIVE_RESPONSE_PATH}/{plugin_id}.py")
    os.chmod(f"{ACTIVE_RESPONSE_PATH}/{plugin_id}.py", 0o750)


def remove_plugin_active_response(plugin_id: str):
    os.remove(f"{ACTIVE_RESPONSE_PATH}/{plugin_id}.py")


async def enable_plugin(metadata: PluginMetadata):
    plugin_id = metadata.id

    add_plugin_rule(plugin_id)
    add_plugin_decoder(plugin_id)
    add_plugin_active_response(plugin_id)

    await asyncio.create_subprocess_shell("systemctl restart wazuh-manager")
    add_plugin_to_agents(plugin_id, metadata.version, metadata.agents)


async def disable_plugin(metadata: PluginMetadata):
    plugin_id = metadata.id

    remove_plugin_rule(plugin_id)
    remove_plugin_decoder(plugin_id)
    remove_plugin_active_response(plugin_id)

    await asyncio.create_subprocess_shell("systemctl restart wazuh-manager")
    remove_plugin_from_agents(plugin_id, metadata.agents)


def validate_plugin_structure(plugin_zip_file: ZipFile):
    """Validates the plugin directory structure to see if all the files are included."""
    for name in filter(lambda name: not name.endswith("/"), plugin_zip_file.namelist()):
        if name not in FULL_PLUGIN_DIR_STRUCTURE:
            raise HTTPException(
                status_code=400,
                detail=f"{name} is not included in the imported plugin.",
            )


async def disable_and_delete_plugin(metadata: PluginMetadata):
    plugin_path = f"{PLUGINS_BASE_DIR}/{metadata.id}"
    metadata_path = f"{plugin_path}/metadata.json"

    with open(metadata_path) as metadata_file:
        metadata = PluginMetadata(**json.loads(metadata_file.read()))

    if metadata.enabled:
        await disable_plugin(metadata)

    shutil.rmtree(plugin_path, ignore_errors=True)


def increment_version(version: str):
    """Increments version patch value ex. 0.0.1 => 0.0.2"""

    version_parts = version.split(".")
    version_parts[2] = str(int(version_parts[2]) + 1)

    return ".".join(version_parts)
