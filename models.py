import re
from typing import List, Optional

from pydantic import validator, BaseModel


class ActiveResponse(BaseModel):
    args: str


class ScriptMetadata(BaseModel):
    interval: int


class PluginMetadata(BaseModel):
    id: Optional[str] = None
    name: str
    description: Optional[str] = None
    version: str
    enabled: bool
    script: ScriptMetadata
    agents: List[str]

    @validator("version")
    def check_semver_version(cls, v: str):
        if not re.match(r"^\d+\.\d+\.\d+$", v):
            raise ValueError("Not a valid version must be in MAJOR.MINOR.PATCH format.")

        return v