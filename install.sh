#!/bin/sh

# Remove previous installation.
rm -rf /opt/socreative-api;

# Extract project code.
cd /opt/;
git clone https://gitlab.com/socreative/socreative-api.git;

# Create python virtual environment.
cd /opt/socreative-api;
python3 -m venv --copies venv;

# Install project dependencies
./venv/bin/pip install -r requirements.txt;

# Create nessesary directories.
mkdir -p /var/ossec/plugins;
mkdir -p /var/ossec/etc/shared/default/plugins;
mkdir -p /var/ossec/active-response/plugins;

# Install our default agent.conf
cp ./assets/agent.conf /var/ossec/etc/shared/default/agent.conf;

# Install api service to systemd.
cp ./assets/socreative-api.service /lib/systemd/system;
