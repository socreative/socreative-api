import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import plugins
from config import SSL_CERTFILE_PATH, SSL_KEYFILE_PATH

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(plugins.router, prefix="/plugins")


@app.get("/")
async def root():
    """Health check endpoint."""
    return {"message": "Hello World"}


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host="0.0.0.0",
        port=55002,
        workers=4,
        ssl_keyfile=SSL_KEYFILE_PATH,
        ssl_certfile=SSL_CERTFILE_PATH,
    )
