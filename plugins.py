import os
import json
import subprocess
import asyncio
from uuid import uuid4
from zipfile import ZipFile

from fastapi.responses import Response, FileResponse
from fastapi import APIRouter, File, HTTPException

from config import (
    PLUGINS_BASE_DIR,
    FULL_PLUGIN_DIR_STRUCTURE,
    MINIMAL_PLUGIN_DIR_STRUCTURE,
    TEMPLATE_PLUGIN_PATH,
)
from utilities import *
from models import ActiveResponse, PluginMetadata


router = APIRouter()


@router.post("/")
async def import_plugin_zipped(file: bytes = File(...)):
    plugin_id = uuid4().hex
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"
    plugin_temp_path = f"/tmp/socreative_{plugin_id}.zip"

    # Store the plugin zip file temporarily.
    with open(plugin_temp_path, "wb") as plugin_temp_file:
        plugin_temp_file.write(file)

    with ZipFile(plugin_temp_path, "r") as plugin_temp_file:

        # Validate the structure of the plugin.
        validate_plugin_structure(plugin_temp_file)

        # Create the plugin directory.
        try:
            os.makedirs(plugin_path, 0o755, False)
        except FileExistsError:
            raise HTTPException(
                status_code=500, detail="UUID4 collision when creating the plugin."
            )

        plugin_temp_file.extractall(plugin_path)

    os.remove(plugin_temp_path)

    with open(f"{plugin_path}/metadata.json") as metadata_file:
        metadata = PluginMetadata(**json.loads(metadata_file.read()))
        metadata.id = plugin_id

    # Write the plugin metadata with the new ID.
    with open(f"{plugin_path}/metadata.json", "w") as metadata_file:
        metadata_file.write(json.dumps(metadata.dict(), indent=2))

    if metadata.enabled:
        await enable_plugin(metadata)

    return metadata


@router.patch("/{plugin_id}.zip")
async def update_plugin_zipped(plugin_id: str, file: bytes = File(...)):
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"
    plugin_temp_path = f"/tmp/socreative_{plugin_id}.zip"

    if not os.path.isdir(plugin_path):
        raise HTTPException(status_code=404, detail="Plugin not found!")

    # Load the old metadata file.
    with open(f"{plugin_path}/metadata.json") as old_metadata_file:
        old_metadata = PluginMetadata(**json.loads(old_metadata_file.read()))

    await disable_and_delete_plugin(old_metadata)

    # Store the plugin zip file temporarily.
    with open(plugin_temp_path, "wb") as plugin_temp_file:
        plugin_temp_file.write(file)

    # Validate and extract the plugin.
    with ZipFile(plugin_temp_path, "r") as plugin_temp_file:

        validate_plugin_structure(plugin_temp_file)
        os.makedirs(plugin_path, 0o755, True)
        plugin_temp_file.extractall(plugin_path)

    os.remove(plugin_temp_path)

    # Make sure that the id in the metadata file is the same as the directory (id/name).
    with open(f"{plugin_path}/metadata.json") as metadata_file:
        metadata = PluginMetadata(**json.loads(metadata_file.read()))
        metadata.id = plugin_id

        # If the version is not updated increment it.
        # The version must be updated for the agents to pick up the update.
        if old_metadata.version == metadata.version:
            metadata.version = increment_version(metadata.version)

    with open(f"{plugin_path}/metadata.json", "w") as metadata_file:
        metadata_file.write(json.dumps(metadata.dict(), indent=2))

    if metadata.enabled:
        await enable_plugin(metadata)

    return metadata


@router.patch("/{plugin_id}.json")
async def update_plugin_metadata(plugin_id: str, metadata: PluginMetadata):
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"

    if not os.path.isdir(plugin_path):
        raise HTTPException(status_code=404, detail="Plugin not found!")

    with open(f"{plugin_path}/metadata.json") as old_metadata_file:
        old_metadata = PluginMetadata(**json.loads(old_metadata_file.read()))

    with open(f"{plugin_path}/metadata.json", "w") as metadata_file:
        metadata.id = plugin_id

        # If the version is not updated increment it.
        # The version must be updated for the agents to pick up the update.
        if old_metadata.version == metadata.version:
            metadata.version = increment_version(metadata.version)

        metadata_file.write(json.dumps(metadata.dict(), indent=2))

    if old_metadata.enabled and metadata.enabled:
        add_plugin_to_agents(plugin_id, metadata.version, metadata.agents)

        new_agents_set = set(metadata.agents)
        old_agents_set = set(old_metadata.agents)
        removed_agents = old_agents_set - (new_agents_set & old_agents_set)

        remove_plugin_from_agents(plugin_id, list(removed_agents))

    elif not old_metadata.enabled and metadata.enabled:
        await enable_plugin(metadata)

    elif old_metadata.enabled and not metadata.enabled:
        await disable_plugin(metadata)

    return metadata


@router.get("/{plugin_id}.json")
async def get_plugin_metadata(plugin_id: str):
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"

    if not os.path.isdir(plugin_path):
        raise HTTPException(status_code=404, detail="Plugin not found!")

    with open(f"{plugin_path}/metadata.json") as metadata_file:
        return json.loads(metadata_file.read())


@router.get("/template-plugin.zip", response_class=FileResponse)
async def get_template_plugin_zipped():
    return FileResponse(TEMPLATE_PLUGIN_PATH, media_type="application/zip")


@router.get("/template-plugins.zip", response_class=FileResponse)
async def get_template_plugin_zipped_with_s():
    return FileResponse(TEMPLATE_PLUGIN_PATH, media_type="application/zip")


@router.get("/{plugin_id}.zip", response_class=FileResponse)
async def get_plugin_zipped(plugin_id: str, size: str = "full"):
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"

    if not os.path.isdir(plugin_path):
        raise HTTPException(status_code=404, detail="Plugin not found!")

    plugin_temp_path = f"/tmp/socreative_{plugin_id}.zip"
    required_structure = (
        MINIMAL_PLUGIN_DIR_STRUCTURE if size == "minimal" else FULL_PLUGIN_DIR_STRUCTURE
    )

    with ZipFile(plugin_temp_path, "w") as plugin_zip_file:
        for filename in required_structure:
            plugin_zip_file.write(f"{plugin_path}/{filename}", arcname=filename)

    return FileResponse(plugin_temp_path, media_type="application/zip")


@router.get("/")
async def list_plugins():
    response = []
    for plugin_directory in os.listdir(PLUGINS_BASE_DIR):
        with open(f"{PLUGINS_BASE_DIR}/{plugin_directory}/metadata.json") as metadata_file:
            metadata = json.loads(metadata_file.read())
            response.append(metadata)

    return response


@router.delete("/{plugin_id}")
async def delete_plugin(plugin_id: str):
    plugin_path = f"{PLUGINS_BASE_DIR}/{plugin_id}"

    if not os.path.isdir(plugin_path):
        raise HTTPException(status_code=404, detail="Plugin not found!")

    with open(f"{plugin_path}/metadata.json") as metadata_file:
        metadata = PluginMetadata(**json.loads(metadata_file.read()))
    
    await disable_and_delete_plugin(metadata)

    return Response(status_code=204)

@router.post("/{plugin_id}/ar")
async def active_response(plugin_id: str, ar: ActiveResponse):
    plugin_ar_path = f"{ACTIVE_RESPONSE_PATH}/{plugin_id}.py"

    if not os.path.exists(plugin_ar_path):
        raise HTTPException(
            404,
            f"Active Response for plugin {plugin_id} doesn't exists or the plugin is not enabled.",
        )

    args = ar.args.strip().split()
    python_args = [ plugin_ar_path ] + args
    await asyncio.create_subprocess_exec(f"python3", *python_args)

    return Response(status_code=204)
