#!/bin/sh

# Stop, disable and remove the API service.
systemctl stop socreative-api.service;
systemctl disable socreative-api.service;

rm /lib/systemd/system/socreative-api.service;

# Remove api git repo.
rm -rf /opt/socreative-api;

# remove plugins system directories.
rm -rf /var/ossec/plugins;
rm -rf /var/ossec/etc/shared/default/plugins;
rm -rf /var/ossec/active-response/plugins;
