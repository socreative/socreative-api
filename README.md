# Roadmap

- [x] Import/Export zipped plugins.
- [x] Retrieve/Update plugins metadata.
- [X] Delete plugins by id.
- [X] List all plugins.
- [X] More validation for imported plugins.
  - [X] Validate imported plugins directory structure.
  - [x] Validate imported plugins metadata.
- [X] Distribute enabled plugins.
  - [x] Add the plugin metadata (id, version) to agents files using `shared` directory.
  - [X] Install decoders and rules for enabled plugins.
  - [X] Install active-response script when plugin is enabled.
- [X] Install script.

# Installation


You need `git` and `python 3.6+` installed before running the installation command.

```bash
sudo sh -c "$(curl -fsSL https://gitlab.com/socreative/socreative-api/-/raw/master/install.sh)"
```

To make the api start at startup.
```bash
$ sudo systemctl enable socreative-api
```

And you need to start the service.
```bash
$ sudo systemctl start socreative-api
```

To uninstall the API.

```bash
$ cd /opt/socreative-api
$ chmod +x uninstall.sh

$ ./uninstall.sh
```

# Directories

## Plugins Directory Structure

The imported plugins directory name is it's ID.

```
/var/ossec/
└── plugins
    └── 0bab811d-dc33-45b8-970b-e15ef64cb12d
        ├── rules.xml
        ├── script.py
        ├── decoders.xml
        ├── active-response
        │   └── script.py
        └── metadata.json
```

### Where things are installed

#### rules.xml and decoders.xml

The content of each file is inserted in the right place in:
- `/var/ossec/etc/rules/local_rules.xml`
- `/var/ossec/etc/decoders/local_decoder.xml`

And using xml comments to identify each plugin and to make it easier to remove them in the future.

#### active-response scripts

Will be install in `/var/ossec/active-response/plugins`
and the directory structure is:

```
/var/ossec/active-response
└── plugins
    ├── 52933f7831fe4ad3945d280f43469966.py
    └── 125c452cd9b44ec88b6c78408f0ccea1.py
```

### `metadata.json` File Structure

- The version needs to be incremented in order for the agents to pull the update.
- The script interval is in **seconds**.

The ID can be excluded when importing new plugins as the API will generate new ID for it and send it back with the response.

```json
{
  "id": "52933f7831fe4ad3945d280f43469966",
  "name": "Plugin Name",
  "description": "Plugin Description",
  "version": "0.0.1",
  "enabled": false,
  "script": {
    "interval": 60
  },
  "agents": ["001", "002"]
}
```

## Agents Directory Structure

For each agent will have a file named id.json where id is the agent id that contain the plugins the agent needs to pull and execute.

When the agent sees a change in it's file in the `shared/default` directory it will go though each plugin defined in it and start comparing the version of the plugins to see what has been added/changed/removed and act accordingly.

```
/var/ossec/etc/shared/default
└── plugins
    ├── 001.json
    └── 002.json
```

### File Structure

```json
[
  {
    "id": "0bab811ddc3345b8970be15ef64cb12d",
    "version": "0.0.1"
  },
  {
    "id": "3cd6bbc9ad8e4d908077f5a45462d647",
    "version": "0.1.0"
  }
]
```

### Pulled Plugin Directory Structure

The agent only needs the script to execute it and the metadata to get the script execution interval and plugin version.

```
plugins
└── 0bab811d-dc33-45b8-970b-e15ef64cb12d
    ├── script.py
    └── metadata.json
```

# Endpoints

- GET `/plugins/`:
  1. Respond with a list of imported plugins metadata.json.
- POST `/plugins/`:
  1. It's expects a plugin zip file to import.
  2. The API will take the plugin and store it.
  3. If the plugin is enabled it will put each component where it belongs as described above.
- GET `/plugins/template-plugin.zip`:
  1. Respond with a template plugin, works as a guide to create new plugins easily.
- GET `/plugins/{plugin_id}.zip?size=minimal`:
  1. Will zip the plugin directory and send it back.
  2. If requested with size minimal will give only script.py and metadata.json in the ip file, used by the `agentd` to pull the plugin for execution.
  3. If requested with size full or without the query string will respond will the full plugin, used by the interface to allow the user to export the plugin.
- POST `/plugins/{plugin_id}.zip`:
  1. re-import the plugin works for the most part like POST `/plugins/`.
  2. Handles changes in the metadata.json.
- GET `/plugins/{plugin_id}.json`:
  1. Respond with the plugin's metadata.json.
- POST `/plugins/{plugin_id}.json`:
  1. Updates the metadata.json file of the plugin.
  2. Handles changes in the metadata.json.
- DELETE `/plugins/{plugin_id}`:
  1. Disable the plugin by removing the components described above appropriately.
  2. Delete the plugin's directory.
- POST `/plugins/{plugin_id}/ar`:
  1. Runs the plugin's active response script.
  2. And gives the script arguments from the request body.


# Handling Changes to the metadata.json

When re-importing the plugin this will happen:

1. Disable the plugin.
2. Remove plugin's the directory.
3. Install the uploaded plugin.
4. Install the plugins components if enabled.

When updating the metadata.json file of the plugin:

- If the plugin is still enabled then see if the agents list is updated, remove the plugin from removed agents and update it in the rest of the agents.
- If wasn't enabled but is enabled now just install the components as above.
- If was enabled and now not enabled just reverse the installation process.
